/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arisa.oop_finalproject;

import java.io.Serializable;

/**
 *
 * @author Asus
 */
public class Fish implements Serializable{
    private String name;
    private int length;
    private int price;
    private String fishing_rod;

    @Override
    public String toString() {
        return "Name of fish : " + name + ",   Length : " + length + " cm "+ ",   Price : " + price +" $"+ ",   Fishing rod :" + fishing_rod + " .";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFishing_rod() {
        return fishing_rod;
    }

    public void setFishing_rod(String fishing_rod) {
        this.fishing_rod = fishing_rod;
    }

    public Fish(String name, int length, int price, String fishing_rod) {
        this.name = name;
        this.length = length;
        this.price = price;
        this.fishing_rod = fishing_rod;
    }
}
